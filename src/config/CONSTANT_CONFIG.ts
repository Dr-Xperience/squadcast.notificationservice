import { config } from './index';
import packageJson from '../../package.json';

const serverName = config.nConfig.get('server:name');
const serverHost = config.nConfig.get('server:host');
const serverBaseUrl = config.nConfig.get('server:url');
const serverPort = config.nConfig.get('server:port');
const defaultTimeZone = config.nConfig.get('server:defaultTimeZone');
const assetsDirectory = config.nConfig.get('paths:assets');
const contentDirectory = config.nConfig.get('paths:content');
const uploadsDirectory = config.nConfig.get('paths:uploads');

const env = config.nConfig.get('env');
const dbHost = config.nConfig.get('database:host');
const dbPort = config.nConfig.get('database:port');
const dbName = config.nConfig.get('database:name');
const dbUser = config.nConfig.get('database:user');
const dbPassword = config.nConfig.get('database:password');
const dbDebug = config.nConfig.get('database:debug');

export const CONSTANT_CONFIG = {
  SERVER: {
    NAME: serverName,
    HOST: serverHost,
    URL: serverBaseUrl,
    PORT: serverPort,
    DEFAULT_TIME_ZONE: defaultTimeZone
  },
  /**
   * Default user agent used in API calls made from this app
   */
  USER_AGENT: `${serverName}/${packageJson.version}`,

  ENV: env,
  DATABASE: {
    HOST: dbHost,
    PORT: dbPort,
    NAME: dbName,
    USER: dbUser,
    PASSWORD: dbPassword,
    DEBUG: dbDebug
  },
  PATHS: {
    ASSETS: assetsDirectory,
    CONTENT: contentDirectory,
    UPLOADS: uploadsDirectory
  }
};
